# serverDB

This is project is a home project to store information about servers in a sqlite database (`.db` file)

## Getting started

Right now is just an idea, so probably it sill be to run the python script into a terminal with any of the following:

```
./serverdb.py
```

OR

```
python serverdb.py
```

## License
The project itself it's licensed with GPL v2 that you can read in the `LICENSE` file

## Project status
This is WIP.
